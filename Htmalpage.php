<?php
    class Htmalpage{
       protected $myTitle = "Default title";
       protected $myBody = "Default body";

        function __construct($title = "",$body = ""){
            if($title != ""){
                $this->myTitle = $title;
            }
            if($body !=""){
                $this->myBody = $body;
            } 
        }

        public function view(){
           echo "<html>
            <head>
                <title> $this->myTitle </title>
            </head>
            <body>
                <p style = 'font-size:$this->size; color:$this->color;'> $this->myBody </p>
            </body>
           </html>"; 
        }
    }

    class coloredHtmalpage extends Htmalpage{
        protected $color; 
        
        public function __set($property,$value){
            if ($property == 'color'){
                $colors = array('pink','yellow','green','blue');
                if (in_array($value,$colors)){
                    $this->color=$value;
                    $this->color;
                }

                else{
                    die("please select one of the allowed colors");
                } 

            }
            
        }
    }

    class fontsizepage extends coloredHtmalpage{
        protected $size;

        public function __set($property,$value){
            parent::__set($property,$value);
            if ($property == 'size'){
                $sizes= array('10','11','12','13','14','15','16','17','18','19','20','21','22','23','24');
                if (in_array($value,$sizes)){
                    $this->size=$value;
                }

                else{
                    die("please select one of the allowed fontsize");
                }
            }
        }
    }


?>


